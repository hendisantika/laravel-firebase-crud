# Laravel Firebase CRUD Customers
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/laravel-firebase-crud.git`.  
2. Go inside the folder: `cd laravel-firebase-crud`.
3. Run `composer install`
4. Run `php artisan key:generate`
5. Run `php artisan serve`

### Screen shot

Index Page

![Index Page](img/index.png "Index Page")

List Customers Page

![List Customers Page](img/list.png "List Customers Page")

Update Customers Page

![Update Customers Page](img/update.png "Update Customers Page")

Firebase Page

![Firebase Page](img/firebase.png "Firebase Page")
